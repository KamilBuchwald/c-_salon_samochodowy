﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualAutoCenter
{
    class Panda : Fiat
    {
          public Panda(int iRokProdukcji) : base(iRokProdukcji)
        {
            inputRokProdukcji = iRokProdukcji;
        }

        public Panda(int iRokProdukcji, float fMarza) : base(iRokProdukcji)
        {
            inputRokProdukcji = iRokProdukcji;
            inputMarza = fMarza;
        }
        public Panda(int iRokProdukcji, float fMarza, float fCenaZakupu) : base(iRokProdukcji)
        {
            inputRokProdukcji = iRokProdukcji;
            inputMarza = fMarza;
            inputCenaZakupu = fCenaZakupu;
        }

        public override string wyswietlModel()
        {
            String st = "Panda";
            return st;
        }
    }
}
