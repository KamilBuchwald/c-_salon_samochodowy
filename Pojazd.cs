﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualAutoCenter
{
     abstract class Pojazd
    {
        protected float inputCenaZakupu, inputMarza;
        protected int inputRokProdukcji;


        public Pojazd(int iRokProdukcji)
        {
            
  
            

            inputRokProdukcji = iRokProdukcji;
        }

        public Pojazd(int iRokProdukcji, float fMarza) : this(iRokProdukcji)
        {
            
            inputMarza = fMarza;
 
        }

        public Pojazd(int iRokProdukcji, float fMarza, float fCenaZakupu) : this(iRokProdukcji,fMarza)
        {

            inputCenaZakupu = fCenaZakupu;
            
        }

        public float Marza
        {
            get
            {
                return inputMarza;
            }
        }

        public float CenaZakupu
        {
            get
            {
                return inputCenaZakupu;
            }
        }

        public int RokProdukcji
        {
            get
            {
                return inputRokProdukcji;
            }
        }




        private void wyswietlRok()
        {

        }

        private float podajCeneKoncowa()
        {
            //Cena koncowa = cena zakupu + marza

            float re = 0;
            return re;
        }

        public abstract String wyswietlMarke();
        public abstract String wyswietlModel();
        public abstract String wyswietlTypPojazdu();
        
     
    }
}
