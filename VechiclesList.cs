﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualAutoCenter
{
    class VechiclesList
    {
        List<Pojazd> listaPojazdow = new List<Pojazd>();

        public VechiclesList()
        {
           
                this.Initialization();
            
        }

        public List<Pojazd> getVechicleList()
        {
            

            return listaPojazdow;
        }


   
        public void sellVechicle(int selectToRemove)
        {

            
            listaPojazdow.RemoveAt(selectToRemove-1);
                 
          

        }


        public void addVechicleToList(Pojazd obiekt){
                listaPojazdow.Add(obiekt);
        }


        private void Initialization()
        {
            listaPojazdow.Add(new Mustang(2007, 25, 120000));
            listaPojazdow.Add(new Ranger(2010, 27, 140000));
            
        }
    }
  
}
