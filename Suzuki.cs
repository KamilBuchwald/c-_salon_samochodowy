using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualAutoCenter
{
    abstract class Suzuki : Motocykl
    {
        public Suzuki(int iRokProdukcji) : base(iRokProdukcji)
        {
        }

        public Suzuki(int iRokProdukcji, float fMarza) : base(iRokProdukcji)
        {

        }
        public Suzuki(int iRokProdukcji, float fMarza, float fCenaZakupu) : base(iRokProdukcji)
        {

        }

        public override string wyswietlMarke()
        {
            String s = "Suzuki";
            return s;
        }
    }
}
