﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualAutoCenter
{
    class Fiat : Samochod
    {
        public Fiat(int iRokProdukcji) : base(iRokProdukcji)
        {
        }

        public Fiat(int iRokProdukcji, float fMarza) : base(iRokProdukcji)
        {

        }
        public Fiat(int iRokProdukcji, float fMarza, float fCenaZakupu) : base(iRokProdukcji)
        {

        }
        public override string wyswietlMarke()
        {
            String s = "Fiat";
            return s;
        }
    }
}
