using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualAutoCenter
{
    class SuperSport : Suzuki
    {
          public SuperSport(int iRokProdukcji) : base(iRokProdukcji)
        {
            inputRokProdukcji = iRokProdukcji;
        }

        public SuperSport(int iRokProdukcji, float fMarza) : base(iRokProdukcji)
        {
            inputRokProdukcji = iRokProdukcji;
            inputMarza = fMarza;
        }
        public SuperSport(int iRokProdukcji, float fMarza, float fCenaZakupu) : base(iRokProdukcji)
        {
            inputRokProdukcji = iRokProdukcji;
            inputMarza = fMarza;
            inputCenaZakupu = fCenaZakupu;
        }

        public override string wyswietlModel()
        {
            String st = "SuperSport";
            return st;
        }
    }
}
