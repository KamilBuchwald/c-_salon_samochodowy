﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualAutoCenter
{
    class Multipla : Fiat
    {
           public Multipla(int iRokProdukcji) : base(iRokProdukcji)
        {
            inputRokProdukcji = iRokProdukcji;
        }

        public Multipla(int iRokProdukcji, float fMarza) : base(iRokProdukcji)
        {
            inputRokProdukcji = iRokProdukcji;
            inputMarza = fMarza;
        }
        public Multipla(int iRokProdukcji, float fMarza, float fCenaZakupu) : base(iRokProdukcji)
        {
            inputRokProdukcji = iRokProdukcji;
            inputMarza = fMarza;
            inputCenaZakupu = fCenaZakupu;
        }

        public override string wyswietlModel()
        {
            String st = "Multipla";
            return st;
        }

    }
}
