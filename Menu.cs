﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.IO;




namespace VirtualAutoCenter


{
    class Menu
    {
        VechiclesList Lista = new VechiclesList();
     
        


        public Menu()
        {
            this.menuMainSection();

        }

        private void menuMainSection()
        {
         
            Console.WriteLine("=========================================");
            Console.WriteLine("                   MENU                  ");
            Console.WriteLine("=========================================");
            Console.WriteLine("                                         ");
            Console.WriteLine("1.Wyświetl listę pojazdów                ");
            Console.WriteLine("2.Dodaj nowy pojazd                      ");
            Console.WriteLine("3.Wyszukaj pojazdy                       ");
            Console.WriteLine("4.Sprzedaj pojazd                        ");
            Console.WriteLine("                                         ");
            Console.WriteLine("=========================================");
            Console.WriteLine("0.Opuść Menu");
            Console.WriteLine("=========================================");


            int choice = 0;

            bool check = false;

            do
            {
                try
                {

                    choice = int.Parse(Console.ReadLine());
                    check = true;
                }
                catch
                {
                    check = false;
                    Console.WriteLine("Podano nieprawidłowy format danych");
                    Console.Clear();
                    this.menuMainSection();

                }
            } while (check == false);

            switch (choice)
            {


                case 1:
                    List<Pojazd> List = Lista.getVechicleList();
                    Console.Clear();
                    Console.WriteLine("=============================================");
                    Console.WriteLine("               WSZYSKIE POJAZDY              ");
                    Console.WriteLine("=============================================");
                    Console.WriteLine("                                             ");
                    
                    for(int i = 0; i<List.Count(); i++)
                    {
                        Console.WriteLine(List[i].wyswietlMarke() + " " + List[i].wyswietlModel() + " " + List[i].RokProdukcji);
                    }
                    Console.WriteLine("                                             ");
                    Console.WriteLine("                                             ");
                    Console.WriteLine("=============================================");
                    Console.WriteLine("                                             ");
                    Console.WriteLine("=============================================");
                    Console.WriteLine("Naciśnij dowolny przycisk aby wrócić do Menu głównego...");
                    Console.ReadKey();
                    Console.Clear();
                    this.menuMainSection();
                    break;

                case 2:
                    Console.Clear();
                    this.addNewVechicle();
                    break;

                case 3:
                    Console.Clear();
                    this.searchVechicle();
                    break;

                case 4:
                    Console.Clear();
                    this.sellVechicle();
                    break;

                default:
                    
                    Console.Clear();
                    this.menuMainSection();


                    break;
            }

        }

            private int checkSearchInputByYear(String input){
            int userInput = 0;
            bool check = false;
            do
            {
                try
                {
                    
                    userInput = int.Parse(input);
                    while(userInput<0){
                        Console.WriteLine("Rok produkcji nie może być mniejszy od 0");
                        Console.WriteLine("Podaj wartość z prawidłowego zakresu: ");
                        userInput = int.Parse(Console.ReadLine());
                        check = false;   
                    }
                    
                    check = true;
                    
                }
                catch
                {
                    check = false;
                    Console.WriteLine("Podano nieprawidłowy format danych - poprawny format to np. 80");
                    Console.WriteLine("Naciśnij dowolny przycisk i wprowadź dane ponownie");
                    Console.ReadKey();
                    this.searchVechicle();
                }
            } while (check == false);

            return userInput;

        }

            private float checkSearchInputByPrice(String input){
            float userInput = 0;
            bool check = false;
            do
            {
                try
                {                   
                    userInput = float.Parse(input);
                    check = true;     
                }
                catch
                {
                    check = false;
                    Console.WriteLine("Podano nieprawidłowy format danych - poprawny format to np. 80");
                    Console.WriteLine("Naciśnij dowolny przycisk i wprowadź dane ponownie");
                    Console.ReadKey();
                    this.searchVechicle();

                }
            } while (check == false);

            return userInput;

        }


        private void searchVechicle()
        {
            Console.Clear();
            List<Pojazd> List = Lista.getVechicleList();
            Console.WriteLine("=============================================");
            Console.WriteLine("                     MENU                    ");
            Console.WriteLine("=============================================");
            Console.WriteLine("                                             ");
            Console.WriteLine("1. Wyszukaj po przedziale cenowym: od do     ");
            Console.WriteLine("2. Wyszukaj po przedziale rocznikowym: od do ");
            Console.WriteLine("                                             ");
            Console.WriteLine("                                             ");
            Console.WriteLine("=============================================");
            Console.WriteLine("0.Wstecz                                     ");
            Console.WriteLine("=============================================");

            int choice = 0;
            bool check = false;
            do
            {
                try
                {
                    Console.WriteLine("Wybierz: ");
                    choice = int.Parse(Console.ReadLine());
                    check = true;
                }
                catch
                {
                    check = false;
                    Console.WriteLine("Podano nieprawidłowy format danych - wprowadź wybór opcji ponownie: ");

                }
            } while (check == false);

            switch (choice)
            {
                case 0:
                    Console.Clear();
                    this.menuMainSection();

                    break;
                case 1:
                    Console.Clear();
                    Console.WriteLine("=============================================");
                    Console.WriteLine("      WYSZUKIWANIE W PRZEDZIALE CENOWYM      ");
                    Console.WriteLine("=============================================");
                    Console.WriteLine("                                             ");
                    Console.WriteLine("Podaj przedział cenowy od: ");
                    float input1 = checkSearchInputByPrice(Console.ReadLine()); //Wymagana walidacja
                    Console.WriteLine("Podaj przedział cenowy do: ");
                    float input2 = checkSearchInputByPrice(Console.ReadLine()); //Wymagana walidacja
                    Console.WriteLine("                                             ");
                    Console.WriteLine("=============================================");
                    Console.Clear();                              
                    Console.WriteLine("=============================================");
                    Console.WriteLine("      WYSZUKIWANIE W PRZEDZIALE CENOWYM      ");
                    Console.WriteLine("      OD: " + input1 + "  DO: " + input2 + " ");
                    Console.WriteLine("=============================================");
                    Console.WriteLine("                                             ");
                    List<Pojazd> znalezione = List.FindAll(x => (x.CenaZakupu > input1 && x.CenaZakupu < input2));
                    for (int i = 0; i<znalezione.Count(); i++)
                    {
                        Console.WriteLine(znalezione[i].wyswietlMarke() + " " + znalezione[i].wyswietlModel() + " Cena auta: " + znalezione[i].CenaZakupu + " Rok produkcji: " + znalezione[i].RokProdukcji );
                    }
                    Console.WriteLine("                                             ");
                    Console.WriteLine("=============================================");

                    
                    
                    Console.WriteLine("Naciśnij dowolny przycisk aby kontynuować...");
                    Console.ReadKey();
                    Console.Clear();
                    this.menuMainSection();

                    break;
                case 2:
                    Console.Clear();
                    Console.WriteLine("=============================================");
                    Console.WriteLine("    WYSZUKIWANIE W PRZEDZIALE ROCZNIKOWYM    ");
                    Console.WriteLine("=============================================");
                    Console.WriteLine("                                             ");
                    Console.WriteLine("Podaj przedział rocznikowy od: ");  //Wymagana walidacja
                    int input3 = checkSearchInputByYear(Console.ReadLine());
                    Console.WriteLine("Podaj przedział rocznikowy do: ");  //Wymagana walidacja
                    int input4 = checkSearchInputByYear(Console.ReadLine());
                    Console.WriteLine("                                             ");
                    Console.WriteLine("=============================================");
                    Console.Clear();                              
                    Console.WriteLine("=============================================");
                    Console.WriteLine("    WYSZUKIWANIE W PRZEDZIALE ROCZNIKOWYM    ");
                    Console.WriteLine("    OD: " + input3 + "    DO: " + input4 + " ");
                    Console.WriteLine("=============================================");
                    Console.WriteLine("                                             ");
                    znalezione = List.FindAll(x => (x.RokProdukcji > input3 && x.RokProdukcji < input4));
                    for (int i = 0; i<znalezione.Count(); i++)
                    {
                        Console.WriteLine(znalezione[i].wyswietlMarke() + " " + znalezione[i].wyswietlModel() + " Cena auta: " + znalezione[i].CenaZakupu + " Rok produkcji: " + znalezione[i].RokProdukcji );
                    }
                    Console.WriteLine("                                             ");
                    Console.WriteLine("=============================================");



                    Console.WriteLine("Naciśnij dowolny przycisk aby kontynuować...");
                    Console.ReadKey();
                    Console.Clear();
                    this.menuMainSection();
                    break;
                default:
                    Console.WriteLine("Brak opcji");
                    Console.WriteLine("Press any key to continue...");
                    Console.ReadKey();
                    Console.Clear();
                    this.menuMainSection();
                    break;
            }

            }

        private void sellVechicle()
        {
            List<Pojazd> listaPojazdow = Lista.getVechicleList();
            Console.WriteLine("=========================================");
            Console.WriteLine("                   MENU                  ");
            Console.WriteLine("=========================================");
            Console.WriteLine("=============SPRZEDAJ POJAZD===========");
            Console.WriteLine("                                         ");
            for(int i = 0; i<listaPojazdow.Count(); i++)
                {
                Console.WriteLine(i+1 + " " + listaPojazdow[i].wyswietlMarke() + " " + listaPojazdow[i].wyswietlModel() + " " + listaPojazdow[i].RokProdukcji);
                }
            Console.WriteLine("                                         ");
            Console.WriteLine("                                         ");
            Console.WriteLine("                                         ");
            Console.WriteLine("                                         ");
            Console.WriteLine("=========================================");
            Console.WriteLine("0.Wstecz                                 ");
            Console.WriteLine("=========================================");

            int choice = 0;
            bool check = false;
            do
            {
                try
                {
                    Console.WriteLine("Wybierz pojazd z listy do sprzedaży: ");
                    choice = int.Parse(Console.ReadLine());
                    check = true;
                }
                catch
                {
                    check = false;
                    Console.WriteLine("Podano nieprawidłowy format danych");

                }
            } while (check == false);

           if(choice == 0){
               Console.Clear();
               this.menuMainSection();
           }

           Lista.sellVechicle(choice);
           Console.WriteLine("Usunięto pojazd z listy");
           Console.WriteLine("Naciśnij dowolny przycisk aby kontynuować");
           Console.ReadKey();
           this.menuMainSection();

        }

        private int checkIsInt(String input){
            int userInput = 0;
            bool check = false;
            do
            {
                try
                {
                    
                    userInput = int.Parse(input);
                    while(userInput > DateTime.Now.Year || userInput < 1899)
                    {
                        Console.WriteLine("Rok produkcji nie może być z przyszłości lub mniejszy niż 1899");
                        Console.WriteLine("Podaj rocznik z prawidłowego zakresu: ");
                        userInput = int.Parse(Console.ReadLine());
                        
                    }
                    
                    check = true;
                    
                }
                catch
                {
                    check = false;
                    Console.WriteLine("Podano nieprawidłowy format danych przykładowy format to np 1000");
                    Console.WriteLine("Naciśnij dowolny przycisk i wprowadź dane ponownie");
                    Console.ReadKey();
                    this.addNewVechicle();

                }
            } while (check == false);

            return userInput;

        }
         private float checkMarza(String input){
            float userInput = 0;
            bool check = false;
            do
            {
                try
                {
                    
                    userInput = float.Parse(input);
                    while(userInput<0 || userInput>100){
                        Console.WriteLine("Marża nie może byc mniejsza od 0 lub większa od 100");
                        Console.WriteLine("Podaj wartość z prawidłowego zakresu: ");
                        userInput = int.Parse(Console.ReadLine());
                           
                    }
                    check = true;
                    
                }
                catch
                {
                    check = false;
                    Console.WriteLine("Podano nieprawidłowy format danych - poprawny format to np. 80");
                    Console.WriteLine("Naciśnij dowolny przycisk i wprowadź dane ponownie");
                    Console.ReadKey();
                    Console.Clear();
                    this.addNewVechicle();

                }
            } while (check == false);

            return userInput;

        }

         private float checkCena(String input){
            float userInput = 0;
            bool check = false;
            do
            {
                try
                {
                    
                    userInput = float.Parse(input);
                    while(userInput<0){
                        Console.WriteLine("Cena nie może być mniejsza niż 0");
                        Console.WriteLine("Podaj cenę większą od 0: ");
                        userInput = int.Parse(Console.ReadLine());
                    }
                    
                    check = true;
                    
                }
                catch
                {
                    check = false;
                    Console.WriteLine("Podano nieprawidłowy format danych - poprawny format to liczba np. 120000");
                    Console.WriteLine("Naciśnij dowolny przycisk i wprowadź dane ponownie");
                    Console.ReadKey();
                    Console.Clear();
                    this.addNewVechicle();

                }
            } while (check == false);

            return userInput;

        }

        private void addNewVechicle()
        {
            
           
            Console.WriteLine("=========================================");
            Console.WriteLine("                   MENU                  ");
            Console.WriteLine("=========================================");
            Console.WriteLine("=============DODAJ NOWY POJAZD===========");
            Console.WriteLine("1. Ford Mustang                          ");
            Console.WriteLine("2. Ford Ranger                           ");
            Console.WriteLine("3. Fiat Panda                            ");
            Console.WriteLine("4. Fiat Multipla                         ");
            Console.WriteLine("5. Suzuki SuperSport                     ");
            Console.WriteLine("                                         ");
            Console.WriteLine("=========================================");
            Console.WriteLine("0.Wstecz                                 ");
            Console.WriteLine("=========================================");

            int choice = 0;
            bool check = false;
            do
            {
                try
                {
                    Console.WriteLine("Wybierz model z listy: ");
                    choice = int.Parse(Console.ReadLine());
                    check = true;
                }
                catch
                {
                    check = false;
                    Console.WriteLine("Podano nieprawidłowy format danych ");

                }
            } while (check == false);

            
                switch(choice){
                    
                    case 0:
                        Console.Clear();
                        this.menuMainSection();

                    break;
                    case 1:

                        Console.Clear();
                        Console.WriteLine("=========================================");
                        Console.WriteLine("               FORD MUSTANG              ");
                        Console.WriteLine("=========================================");
                        Console.WriteLine("                                         ");
                        Console.WriteLine("Podaj rocznik pojzdu:  (np. 2014)        ");
                        int inputRocznik = checkIsInt(Console.ReadLine());
                        Console.WriteLine("                                         ");
                        Console.WriteLine("Podaj marże pojzdu: (do 100) ");
                        float inputMarza = checkMarza(Console.ReadLine());
                        Console.WriteLine("                                         ");
                        Console.WriteLine("Podaj cenę pojzdu: (np. 25000) ");
                        float inputCena = checkCena(Console.ReadLine());

                        Console.WriteLine("                                         ");
                        Console.WriteLine("=========================================");
                        Console.WriteLine("0.Wstecz                                 ");
                        Console.WriteLine("=========================================");

                   
                        
                        Lista.addVechicleToList(new Mustang(inputRocznik,inputMarza,inputCena));
                        Console.Clear();
                        
                        Console.WriteLine("=========================================");
                        Console.WriteLine("      DODANO NOWY POJAZD DO SALONU       ");
                        Console.WriteLine("=========================================");
                        Console.WriteLine("                                         ");                        
                        Console.WriteLine("Marka: Ford");
                        Console.WriteLine("Model: Mustang");
                        Console.WriteLine("Rocznik: " + inputRocznik );
                        Console.WriteLine("Marża: " + inputMarza + "%" );
                        Console.WriteLine("Cena pojazdu: " + inputCena + " zł" );
                        Console.WriteLine("                                         ");
                        Console.WriteLine("=========================================");
                        Console.WriteLine("Naciśnij dowolny przycisk aby wrócić do Menu");
                        Console.WriteLine("=========================================");

                        Console.ReadKey();
                        this.menuMainSection();


                    break;
                     

                    case 2:

                        Console.Clear();
                        Console.WriteLine("=========================================");
                        Console.WriteLine("               FORD RANGER               ");
                        Console.WriteLine("=========================================");
                        Console.WriteLine("                                         ");
                        Console.WriteLine("Podaj rocznik pojzdu:  (np. 2014)        ");
                        inputRocznik = checkIsInt(Console.ReadLine());
                        Console.WriteLine("                                         ");
                        Console.WriteLine("Podaj marże pojzdu: (do 100) ");
                        inputMarza = checkMarza(Console.ReadLine());
                        Console.WriteLine("                                         ");
                        Console.WriteLine("Podaj cenę pojzdu: (np. 25000) ");
                        inputCena = checkCena(Console.ReadLine());

                        Console.WriteLine("                                         ");
                        Console.WriteLine("=========================================");
                        Console.WriteLine("0.Wstecz                                 ");
                        Console.WriteLine("=========================================");

                   
                        Lista.addVechicleToList(new Ranger(inputRocznik,inputMarza,inputCena));
                        Console.Clear();
                        
                        Console.WriteLine("=========================================");
                        Console.WriteLine("      DODANO NOWY POJAZD DO SALONU       ");
                        Console.WriteLine("=========================================");
                        Console.WriteLine("                                         ");                        
                        Console.WriteLine("Marka: Ford");
                        Console.WriteLine("Model: Ranger");
                        Console.WriteLine("Rocznik: " + inputRocznik );
                        Console.WriteLine("Marża: " + inputMarza + "%");
                        Console.WriteLine("Cena pojazdu: " + inputCena + " zł" );
                        Console.WriteLine("                                         ");
                        Console.WriteLine("=========================================");
                        Console.WriteLine("Naciśnij dowolny przycisk aby wrócić do Menu");
                        Console.WriteLine("=========================================");

                        Console.ReadKey();
                        Console.Clear();
                        this.menuMainSection();

                    break;

                    case 3: 

                        Console.Clear();
                        Console.WriteLine("=========================================");
                        Console.WriteLine("                FIAT PANDA               ");
                        Console.WriteLine("=========================================");
                        Console.WriteLine("                                         ");
                        Console.WriteLine("Podaj rocznik pojzdu:  (np. 2014)        ");
                        inputRocznik = checkIsInt(Console.ReadLine());
                        Console.WriteLine("                                         ");
                        Console.WriteLine("Podaj marże pojzdu: (do 100) ");
                        inputMarza = checkMarza(Console.ReadLine());
                        Console.WriteLine("                                         ");
                        Console.WriteLine("Podaj cenę pojzdu: (np. 25000) ");
                        inputCena = checkCena(Console.ReadLine());

                        Console.WriteLine("                                         ");
                        Console.WriteLine("=========================================");
                        Console.WriteLine("0.Wstecz                                 ");
                        Console.WriteLine("=========================================");

                   
                        Lista.addVechicleToList(new Panda(inputRocznik,inputMarza,inputCena));
                    
                        Console.Clear();
                        
                        Console.WriteLine("=========================================");
                        Console.WriteLine("      DODANO NOWY POJAZD DO SALONU       ");
                        Console.WriteLine("=========================================");
                        Console.WriteLine("                                         ");                        
                        Console.WriteLine("Marka: Fiat");
                        Console.WriteLine("Model: Panda");
                        Console.WriteLine("Rocznik: " + inputRocznik );
                        Console.WriteLine("Marża: " + inputMarza + "%" );
                        Console.WriteLine("Cena pojazdu: " + inputCena + " zł" );
                        Console.WriteLine("                                         ");
                        Console.WriteLine("=========================================");
                        Console.WriteLine("Naciśnij dowolny przycisk aby wrócić do Menu");
                        Console.WriteLine("=========================================");

                        Console.ReadKey();
                        Console.Clear();
                        this.menuMainSection();
                    break;

                case 4: 

                        Console.Clear();
                        Console.WriteLine("=========================================");
                        Console.WriteLine("              FIAT MULTIPLA              ");
                        Console.WriteLine("=========================================");
                        Console.WriteLine("                                         ");
                        Console.WriteLine("Podaj rocznik pojzdu:  (np. 2014)        ");
                        inputRocznik = checkIsInt(Console.ReadLine());
                        Console.WriteLine("                                         ");
                        Console.WriteLine("Podaj marże pojzdu: (do 100) ");
                        inputMarza = checkMarza(Console.ReadLine());
                        Console.WriteLine("                                         ");
                        Console.WriteLine("Podaj cenę pojzdu: (np. 25000) ");
                        inputCena = checkCena(Console.ReadLine());

                        Console.WriteLine("                                         ");
                        Console.WriteLine("=========================================");
                        Console.WriteLine("0.Wstecz                                 ");
                        Console.WriteLine("=========================================");

                   
                        Lista.addVechicleToList(new Multipla(inputRocznik,inputMarza,inputCena));
                        Console.Clear();
                        
                        Console.WriteLine("=========================================");
                        Console.WriteLine("      DODANO NOWY POJAZD DO SALONU       ");
                        Console.WriteLine("=========================================");
                        Console.WriteLine("                                         ");                        
                        Console.WriteLine("Marka: Fiat");
                        Console.WriteLine("Model: Multipla");
                        Console.WriteLine("Rocznik: " + inputRocznik );
                        Console.WriteLine("Marża: " + inputMarza + "%" );
                        Console.WriteLine("Cena pojazdu: " + inputCena + " zł" );
                        Console.WriteLine("                                         ");
                        Console.WriteLine("=========================================");
                        Console.WriteLine("Naciśnij dowolny przycisk aby wrócić do Menu");
                        Console.WriteLine("=========================================");

                        Console.ReadKey();
                        Console.Clear();
                        this.menuMainSection();
                    break;
                case 5: 

                        Console.Clear();
                        Console.WriteLine("=========================================");
                        Console.WriteLine("           SUZUKI SUPERSPORT             ");
                        Console.WriteLine("=========================================");
                        Console.WriteLine("                                         ");
                        Console.WriteLine("Podaj rocznik pojzdu:  (np. 2014)        ");
                        inputRocznik = checkIsInt(Console.ReadLine());
                        Console.WriteLine("                                         ");
                        Console.WriteLine("Podaj marże pojzdu: (do 100) ");
                        inputMarza = checkMarza(Console.ReadLine());
                        Console.WriteLine("                                         ");
                        Console.WriteLine("Podaj cenę pojzdu: (np. 25000) ");
                        inputCena = checkCena(Console.ReadLine());

                        Console.WriteLine("                                         ");
                        Console.WriteLine("=========================================");
                        Console.WriteLine("0.Wstecz                                 ");
                        Console.WriteLine("=========================================");

                   
                        Lista.addVechicleToList(new SuperSport(inputRocznik,inputMarza,inputCena));
                        Console.Clear();
                        
                        Console.WriteLine("=========================================");
                        Console.WriteLine("      DODANO NOWY POJAZD DO SALONU       ");
                        Console.WriteLine("=========================================");
                        Console.WriteLine("                                         ");                        
                        Console.WriteLine("Marka: Suzuki");
                        Console.WriteLine("Model: SuperSport");
                        Console.WriteLine("Rocznik: " + inputRocznik );
                        Console.WriteLine("Marża: " + inputMarza + "%" );
                        Console.WriteLine("Cena pojazdu: " + inputCena + " zł" );
                        Console.WriteLine("                                         ");
                        Console.WriteLine("=========================================");
                        Console.WriteLine("Naciśnij dowolny przycisk aby wrócić do Menu");
                        Console.WriteLine("=========================================");

                        Console.ReadKey();
                        Console.Clear();
                        this.menuMainSection();
                    break;            
                default:
                    Console.WriteLine("Podano nieprawidłowy parametr! Nacisnij dowolny przycisk i spróbuj ponownie");
                    Console.ReadKey();
                    this.addNewVechicle();
                    break;    
                }
                    

                    

 

        }




        
    }
}
