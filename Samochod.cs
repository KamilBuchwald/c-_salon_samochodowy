﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualAutoCenter
{
    abstract class Samochod : Pojazd
    {

        public Samochod(int iRokProdukcji) : base(iRokProdukcji)
        {
           
        }

        public Samochod(int iRokProdukcji, float fMarza) : base(iRokProdukcji)
        {
         
        }
        public Samochod(int iRokProdukcji, float fMarza, float fCenaZakupu) : base(iRokProdukcji)
        {
         
        }

        public override string wyswietlMarke()
        {
            String s = "Wyswietl Marke";
            return s;
        }

        public override string wyswietlModel()
        {
            String st = "Wyswietl Model";
            return st;
        }

        public override string wyswietlTypPojazdu()
        {
            String str = "Wyswietl typ Pojazdu";
            return str;
        }

        
    }
}
